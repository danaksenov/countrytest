package com.example.countriesexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TableRow;
import android.widget.Toast;

import com.example.countriesexample.Network.ApiService;
import com.example.countriesexample.Network.CountryAdapter;
import com.example.countriesexample.Network.model.Country;
import com.example.countriesexample.databinding.ActivityMainBinding;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class MainActivity extends AppCompatActivity {
private Disposable disposable;
private CountryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.example.countriesexample.databinding.ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        disposable = ApiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showInfo, this::showError);
    }
    public void showInfo(List<Country> countries){
        //Toast.makeText(MainActivity.this, countries.get(0).name, Toast.LENGTH_SHORT).show();
        adapter.update(countries);
    }

    public void showError(Throwable t){
        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onStop() {
        super.onStop();
        if(disposable !=null){
            disposable.dispose();
        }
    }
}
